﻿// See https://aka.ms/new-console-template for more information
using SwedenIntroToEF.DataAccess;
using SwedenIntroToEF.Models;


// dependencies ✔️

// model classes ✔️

// dbcontext ✔️

// create migration ✔️

// update-database ✔️

// success


// CRUD Operations:

// Create new prof ✔️
// Get all, by Id, by Name ✔️
// Update prof by Id ✔️
// Delete by Id

static List<Professor> GetAllProfessors()
{
    using PostGradDbContext db = new();

    List<Professor> professors = db.Professors.ToList();

    return professors;
}

static Professor GetProfessorById(int id)
{
    using PostGradDbContext db = new();

    Professor result = db.Professors.FirstOrDefault(p => p.Id == id);

    return result;
}

static List<Professor> GetProfessorByName(string name)
{
    using PostGradDbContext db = new();

    List<Professor> result = db.Professors.Where(prof => prof.FirstName.Contains(name)).ToList();

    return result;
}

static void AddNewProfessor(Professor professor)
{
    using PostGradDbContext db = new();

    db.Professors.Add(professor);

    db.SaveChanges();
}

static void UpdateProfessor(int id, Professor updatedProfessor)
{
    using PostGradDbContext db = new();

    try
    {
        Professor profToUpdate = db.Professors.First(prof => prof.Id == id);

        profToUpdate.FirstName = updatedProfessor.FirstName;
        profToUpdate.LastName = updatedProfessor.LastName;
        profToUpdate.Subject = updatedProfessor.Subject;

        db.SaveChanges();
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
}

static void DeleteProfessorById(int id)
{
    using PostGradDbContext db = new();

    Professor profToDelete = db.Professors.FirstOrDefault(prof => prof.Id == id);

    db.Professors.Remove(profToDelete);

    db.SaveChanges();
}


// Helper display methods
static void DisplayProfessors(List<Professor> professors)
{
    foreach (Professor prof in professors)
    {
        Console.WriteLine($"{prof.Id} {prof.FirstName} {prof.LastName} {prof.Subject}");
    }
}

static void DisplayProfessor(Professor prof)
{
    Console.WriteLine($"{prof.Id} {prof.FirstName} {prof.LastName} {prof.Subject}");

}

// Get and display all professors

Console.WriteLine("Get all professors:");
DisplayProfessors(GetAllProfessors());

//Console.WriteLine("\nGet Prof by id (id=1)");
//DisplayProfessor(GetProfessorById(1));

Console.WriteLine("\nGet Profs by name (name=e)");
DisplayProfessors(GetProfessorByName("e"));

//Console.WriteLine("\nAdd new prof (Sean Skinner JavaScript)");

//Professor newProf = new()
//{
//    FirstName = "Sean",
//    LastName = "Skinner",
//    Subject = "JavaScript",
//};

//AddNewProfessor(newProf);

//DisplayProfessors(GetAllProfessors());

Console.WriteLine("\nUpdate prof (Sean Skinner: JavaScript -> .NET)");

Professor updatedProf = new()
{
    Id = 3,
    FirstName = "Sean",
    LastName = "Skinner",
    Subject = ".NET",
};

UpdateProfessor(3, updatedProf);

DisplayProfessors(GetAllProfessors());

//Console.WriteLine("\nDelete prof (4)");

//DeleteProfessorById(4);
//DisplayProfessors(GetAllProfessors());