﻿using System.ComponentModel.DataAnnotations;

namespace SwedenIntroToEF.Models
{
    internal class ProfessorQualification
    {
        // Custom Properties
        [MaxLength(4)]
        public string YearEarned { get; set; }
        // Composite Primary / Foreign Keys
        public int ProfessorId { get; set; }
        public int QualificationId { get; set; }

        // Navigation Properties
        public Professor Professor { get; set; }
        public Qualification Qualification { get; set; }
    }
}
