﻿using SwedenIntroToEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwedenIntroToEF.DataAccess
{
    internal static class SeedHelper
    {
        public static List<Professor> GetProfessorSeeds()
        {
            List<Professor> newProfessors = new() {
                new Professor() { Id = 1, FirstName = "Warren", LastName = "West", Subject = "JavaScript" },
                new Professor() { Id = 2, FirstName = "Dean", LastName = "von Schoultz", Subject = "C#" },
                new Professor() { Id = 3, FirstName = "Reza", LastName = "Mirzaei", Subject = "Java" },
            };

            return newProfessors;
        }

        public static List<Qualification> GetQualificationSeeds()
        {
            List<Qualification> newQualificationSeeds = new() {
                new Qualification() { Id = 1, Name = "BTech Information Technology" },
                new Qualification() { Id = 2, Name = "Masters in Information Technology" },
                new Qualification() { Id = 3, Name = "PhD in Information Technology" },
                new Qualification() { Id = 4, Name = "BSc in Computer Science" },
                new Qualification() { Id = 5, Name = "Masters in Computer Science" },
            };

            return newQualificationSeeds;
        }

        public static List<ProfessorQualification> GetProfessorQualificationSeeds()
        {
            List<ProfessorQualification> profQualSeeds = new() {
                new ProfessorQualification() { ProfessorId = 1, QualificationId = 1, YearEarned = "2015" },
                new ProfessorQualification() { ProfessorId = 2, QualificationId = 1, YearEarned = "2014" },
                new ProfessorQualification() { ProfessorId = 2, QualificationId = 2, YearEarned = "2020" },
                new ProfessorQualification() { ProfessorId = 2, QualificationId = 3, YearEarned = "2023" },
                new ProfessorQualification() { ProfessorId = 3, QualificationId = 4, YearEarned = "2012" },
                new ProfessorQualification() { ProfessorId = 3, QualificationId = 5, YearEarned = "2016" },
            };

            return profQualSeeds;
        }
    }
}
