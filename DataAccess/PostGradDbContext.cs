﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using SwedenIntroToEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwedenIntroToEF.DataAccess
{
    internal class PostGradDbContext : DbContext
    {
        public DbSet<Professor> Professors { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Qualification> Qualifications { get; set; }
        public DbSet<ProfessorQualification> ProfessorQualifications { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(GetConnectionString());
            //optionsBuilder.UseSqlServer("Data Source=localhost\\SQLEXPRESS; Initial Catalog=PostGradDb; Integrated Security=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProfessorQualification>().HasKey(pq => new { pq.ProfessorId, pq.QualificationId });

            // Seeding domain entity tables
            modelBuilder.Entity<Professor>().HasData(SeedHelper.GetProfessorSeeds());
            modelBuilder.Entity<Qualification>().HasData(SeedHelper.GetQualificationSeeds());

            // Seeding many to many
            modelBuilder.Entity<ProfessorQualification>().HasData(SeedHelper.GetProfessorQualificationSeeds());

            //modelBuilder.Entity<Professor>()
            //    .HasMany(prof => prof.Qualifications)
            //    .WithMany(qual => qual.Professors)
            //    .UsingEntity<Dictionary<string, object>>(
            //        "ProfessorQualifications",
            //        r => r.HasOne<Qualification>().WithMany().HasForeignKey("QualificationsId"),
            //        l => l.HasOne<Professor>().WithMany().HasForeignKey("ProfessorsId"),
            //        je =>
            //        {
            //            je.HasKey("ProfessorsId", "QualificationsId");
            //            je.HasData(SeedHelper.GetProfessorQualificationSeeds());
            //        }
            //    );
        }

        private string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new()
            {
                DataSource = "localhost\\SQLEXPRESS",
                InitialCatalog = "PostGradDb",
                IntegratedSecurity = true,
                TrustServerCertificate = true,
            };

            return builder.ConnectionString;
        }
    }
}
