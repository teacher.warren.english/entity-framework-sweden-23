﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace SwedenIntroToEF.Migrations
{
    /// <inheritdoc />
    public partial class CorrectedProfQualRelationship : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProfessorQualification");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProfessorQualifications",
                table: "ProfessorQualifications");

            migrationBuilder.DropIndex(
                name: "IX_ProfessorQualifications_ProfessorId",
                table: "ProfessorQualifications");

            migrationBuilder.DropColumn(
                name: "ProfessorsId",
                table: "ProfessorQualifications");

            migrationBuilder.DropColumn(
                name: "QualificationsId",
                table: "ProfessorQualifications");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Qualifications",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProfessorQualifications",
                table: "ProfessorQualifications",
                columns: new[] { "ProfessorId", "QualificationId" });

            migrationBuilder.InsertData(
                table: "ProfessorQualifications",
                columns: new[] { "ProfessorId", "QualificationId", "YearEarned" },
                values: new object[,]
                {
                    { 1, 1, "2015" },
                    { 2, 1, "2014" },
                    { 2, 2, "2020" },
                    { 2, 3, "2023" },
                    { 3, 4, "2012" },
                    { 3, 5, "2016" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ProfessorQualifications",
                table: "ProfessorQualifications");

            migrationBuilder.DeleteData(
                table: "ProfessorQualifications",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "ProfessorQualifications",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 2, 1 });

            migrationBuilder.DeleteData(
                table: "ProfessorQualifications",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 2, 2 });

            migrationBuilder.DeleteData(
                table: "ProfessorQualifications",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 2, 3 });

            migrationBuilder.DeleteData(
                table: "ProfessorQualifications",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 3, 4 });

            migrationBuilder.DeleteData(
                table: "ProfessorQualifications",
                keyColumns: new[] { "ProfessorId", "QualificationId" },
                keyValues: new object[] { 3, 5 });

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Qualifications",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100);

            migrationBuilder.AddColumn<int>(
                name: "ProfessorsId",
                table: "ProfessorQualifications",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "QualificationsId",
                table: "ProfessorQualifications",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProfessorQualifications",
                table: "ProfessorQualifications",
                columns: new[] { "ProfessorsId", "QualificationsId" });

            migrationBuilder.CreateTable(
                name: "ProfessorQualification",
                columns: table => new
                {
                    ProfessorsId = table.Column<int>(type: "int", nullable: false),
                    QualificationsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfessorQualification", x => new { x.ProfessorsId, x.QualificationsId });
                    table.ForeignKey(
                        name: "FK_ProfessorQualification_Professors_ProfessorsId",
                        column: x => x.ProfessorsId,
                        principalTable: "Professors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProfessorQualification_Qualifications_QualificationsId",
                        column: x => x.QualificationsId,
                        principalTable: "Qualifications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "ProfessorQualification",
                columns: new[] { "ProfessorsId", "QualificationsId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 2, 2 },
                    { 2, 3 },
                    { 3, 4 },
                    { 3, 5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProfessorQualifications_ProfessorId",
                table: "ProfessorQualifications",
                column: "ProfessorId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfessorQualification_QualificationsId",
                table: "ProfessorQualification",
                column: "QualificationsId");
        }
    }
}
